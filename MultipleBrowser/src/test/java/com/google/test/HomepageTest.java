package com.google.test;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.page.HompagePage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class HomepageTest extends WebDriverTestCase {
 
	@Parameters("browser")
	@Test(description="Google testing", enabled=false)
	public void verifyGoogle() {
		HompagePage hompagePage = new HompagePage();
		hompagePage.launchPage(null);
		hompagePage.enterGoogle();
		
	}
	@Test
	public void verifyText() {
		HompagePage hompagePage = new HompagePage();
		hompagePage.launchPage(null);
		hompagePage.enterText();
		
	}
	@Test
	public void verifyMicrosoft() {
		HompagePage hompagePage = new HompagePage();
		hompagePage.launchPage(null);
		hompagePage.enterMicrosoft();
		
	}
	
}
