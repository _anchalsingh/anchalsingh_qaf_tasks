package com.google.page;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HompagePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator="homepage.search.text")
	private QAFWebElement searchField;
	
	@FindBy(locator="homepage.googlesearch.btn")
	private QAFWebElement googleBtn;
	
	public void enterText() {
		searchField.sendKeys("Text");
		googleBtn.click();
		driver.navigate().back();
		waitForPageToLoad();
		System.out.println(Thread.currentThread().getId());
		//Comments to see if its updated
	}
	
	public void enterGoogle() {
		searchField.sendKeys("google");
		googleBtn.click();
		driver.navigate().back();
		waitForPageToLoad();
		System.out.println(Thread.currentThread().getId());
	
	}
	
	public void enterMicrosoft() {
		searchField.sendKeys("Microsoft");
		googleBtn.click();
		driver.navigate().back();
		waitForPageToLoad();
		System.out.println(Thread.currentThread().getId());
	
	}
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		driver.get("/");
	}

}
